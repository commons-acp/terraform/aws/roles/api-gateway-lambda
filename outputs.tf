output "lambda_role" {
  value = aws_iam_role.iam_for_lambda.arn
}
output "apigw_role" {
  value       = aws_iam_role.iam_for_api_gw.arn
  description = "The Amazon Resource Name (ARN) specifying the role."
}
